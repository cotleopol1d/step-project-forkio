https://cotleopol1d.gitlab.io/step-project-forkio/
## Project participants:
- Viktoriia Grankina (vikgrnk@gmail.com);
- Vitaliy Talko (cotleopol1d@gmail.com).
## Task distribution:
- Viktoriia Grankina made 'header' section and 'people-are-talking' section;
- Vitaliy Talko made 'revolutionary-editor' block, 'prises' and 'what-you-get' sections;
- Setting up gulp-tasks, setting up the repository and setting up Gitlab-pages was a teamwork.
## List of technologies used:
- Teamwork realised with git-technology and gitlab repository. Each student developed their own tasks on their own 
git-branches. Branch 'dev' used for testing and debugging. Final version of project placed on the 'main' branch.
- The project was developed using gulp;
- List of used dependencies for gulp you can find in a package.json;
- The project was placed on the Internet using Gitlab-pages;



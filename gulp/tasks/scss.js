import gulpSass from "gulp-sass";
import dartSass from "sass";
import rename from "gulp-rename";
import cleanCss from "gulp-clean-css";
import webpCss from 'gulp-webpcss';
import autoprefixer from "gulp-autoprefixer";
import groupMedia from "gulp-group-css-media-queries";
import uncss from "gulp-uncss"
const sass = gulpSass(dartSass);
export const scss = (done)=> {
    app.gulp.src(app.path.src.scss, {sourcemaps: app.isDev})
        .pipe(app.plugins.plumber(app.plugins.notify.onError({
            title: "SCSS",
            massage: "Error: <%= error.message %>"
        })))
        .pipe(sass({
            outputStyle: "expanded"
        }))
        .pipe( webpCss({
            webpClass: ".webp",
            noWebpClass: ".no-webp"
        }))
        .pipe(
            app.plugins.if(
                app.isBuild,groupMedia()))
        .pipe(autoprefixer({
            grid:true,
            overrideBrowserslist: ["last 3 versions"],
            cascade: true,
        }))
        .pipe(app.plugins.if(
            app.isBuild,
            cleanCss()))
        .pipe(rename({
            extname: ".min.css"
        }))
        .pipe(uncss({
            html: ['index.html'],
            ignore: [/\.webp/,/\.no-webp/, /\.header-nav__list--open/, /\.burger-btn--open/],
        }))
        .pipe(app.gulp.dest(app.path.build.css))
        .pipe(app.plugins.browserSync.stream());
    done();
}